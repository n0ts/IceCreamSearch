/*
 * @Author: N0ts
 * @Date: 2022-11-21 14:25:06
 * @Description: 背景图片工具
 * @FilePath: /vue/src/utils/backgroup.ts
 * @Mail：mail@n0ts.top
 */

import http from "@/utils/http/axios";
import data from "@/data/data";
import notify from "@/utils/notify/notify";

export default {
    /**
     * 获取 Bing 壁纸
     */
    getBing() {
        http.post("https://api.n0ts.top/cors", {
            method: "get",
            url: "https://cn.bing.com/HPImageArchive.aspx",
            params: {
                format: "js",
                idx: 0,
                n: 8,
                mkt: "zh-CN"
            }
        })
            .then((res) => {
                data.bingData = res.data.images;
                // 如果已经设置壁纸则跳过
                if (data.saveData.bgLink) {
                    return this.setBg(data.saveData.bgLink);
                }

                // 设置第一张图片
                data.saveData.bingIndex = 0;
                data.saveData.bgLinkContent = data.bingData[0].copyright;
                data.saveData.bgLinkHref = data.bingData[0].copyrightlink;
                data.saveData.bgLink =
                    "https://cn.bing.com/" + data.bingData[0].url;
            })
            .catch((err) => {
                notify("获取 bing 壁纸失败", 4);
            });
    },

    /**
     * 设置背景样式
     */
    setBg(url: string) {
        data.backgroundCss = `background: url("${url}") center center / cover fixed;`;
    }
};
