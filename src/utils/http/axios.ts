/*
 * @Author: N0ts
 * @Date: 2022-01-11 09:48:26
 * @LastEditTime: 2022-11-24 15:40:55
 * @Description: Axios 封装
 * @FilePath: /vue/src/utils/http/axios.ts
 * @Mail：mail@n0ts.top
 */

import axios from "axios";
import data from "@/data/data";

let httpNum = 0;

// 添加请求拦截器
axios.interceptors.request.use(
    function (config) {
        httpNum++;
        data.loading = true;
        return config;
    },
    function (error) {
        return Promise.reject(error);
    }
);

// 添加响应拦截器
axios.interceptors.response.use(
    function (response) {
        httpNum--;
        if (httpNum == 0) {
            data.loading = false;
        }
        return response.data;
    },
    function (error) {
        httpNum--;
        if (httpNum == 0) {
            data.loading = false;
        }
        return Promise.reject(error);
    }
);

export default axios;
