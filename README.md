# QQ 群讨论

[坚果茶馆](https://jq.qq.com/?_wv=1027&k=Mh7ah6Dd)

# IceCreamSearch

### 介绍

简约风格的搜索主页，内置各类网址收藏，好看才是第一生产力！  

已重构完成

预览地址：[IceCream 冰激凌 | 简约至上 (n0ts.top)](https://search.n0ts.top/)

## 支持我

[点我点我☕️](https://support.n0ts.top/)